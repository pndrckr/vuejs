
Vue.component('tabs',{
	template: `
			<div>
				<div class="tabs is-centered">
					<ul>
						<li v-for="tab in tabs" :class="{ 'is-active': tab.isActive }">
							<a :href="tab.href" @click="selectTab(tab)"> {{ tab.name }} </a>
						</li>
					</ul>
				</div>

				<div class="tab-details">
					<slot></slot>
				</div>
			</div>
	`,

	// mounted() {
	// 	console.log(this.$children);
	// }
	data() {
		// defino tabs para usar despues en created()
		return { tabs: [] };
	},	

	created() {
		// populo el array que definí en data()
		this.tabs = this.$children;
		
	},

	methods: {
		selectTab(selectedTab) {
			this.tabs.forEach(tab => {
				tab.isActive = (tab.name == selectedTab.name);
			});
		}
	}

});




Vue.component('tab',{

	props: { 	
		name: { required : true},
		selected: {default: false} 
	},

	template: `
	<div v-show="isActive"><slot></slot></div>
	`,

	data() {
		return {
			isActive: true
		}
	},

	computed: {
		href() {
			return '#'+this.name.toLowerCase().replace(/ /g, '-');
		}
	},

	mounted() {
		this.isActive = this.selected;
	}
});




new Vue({
	el:'#root'
});