Vue.component('coupon', {
	template: `

  <p class="control has-icons-right">
    <input class="input is-rounded" type="text" @blur="grito">
    <span class="icon is-small is-right">
    </span>
  </p>

	`,

	methods: {
		grito() {
			this.$emit('applied');
		}
	}
});

new Vue({
	el:'#root',

	data: {
		couponApplied: false
	},

	methods: {
		// Respuesta al grito 'applied':
		onCouponApplied() {
			alert('respondo al grito');
			// if (!this.$el.childNodes[0].value) this.couponApplied = true;
			if (this.$el.childNodes[0].value == "") {
				this.couponApplied = false;
			} else { 
				this.couponApplied = true;
			}
		}
	}
});