Vue.component('saraza', {

	template: '<h1><slot></slot></h1>'


});

Vue.component('saraza-data', {

	template: '<div><saraza v-for="saludo in saludos">{{ saludo.text }}</saraza></div>',
		// Cuando no tenemos un solo elemento raiz para solucionarlo basta con envolverlo en un div.

	data() {
		return {
			saludos: [
				{ text: 'Hello World!' },
				{ text: 'Hola Mundo!' },
				{ text: 'Ciao Mondo!' },
				{ text: 'Bonjour Monde!' }
			]
		};
	}

});


new Vue({
	el:'#root'
});