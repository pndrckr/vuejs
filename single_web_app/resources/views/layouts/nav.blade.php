<nav class="nav nav-masthead justify-content-center">
	<router-link to='/' exact class="nav-link">Home</router-link>
	<router-link to='/about' class="nav-link">About</router-link>
	<router-link to='/features' class="nav-link">Features</router-link>
	<router-link to='/tweets' class="nav-link">Tweets</router-link>
</nav>