<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/app.css">
    <title>My SWA</title>

</head>
<body class="text-center">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

        <div id="app">
        @include ('layouts.header')
        
            <main role="main" class="inner cover">
                <router-view></router-view>
            </main>

        @include ('layouts.footer')
        </div>

    </div>
    <script src="/js/app.js"></script>        
</body>
</html>


