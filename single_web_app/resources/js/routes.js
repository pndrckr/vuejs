import VueRouter from 'vue-router';

let routes = [
	{
		path: '/',
		name: 'Home',
		component: require('./views/Home.vue').default
	},
	{
		path: '/about',
		name: 'About',
		component: require('./views/About').default
	},
	{
		path: '/features',
		name: 'Features',
		component: require('./views/Features').default
	},
	{
		path: '/tweets',
		name: 'Tweets',
		component: require('./views/Tweets').default
	}
];

export default new VueRouter({
	routes,
	linkActiveClass: 'active'
});