Vue.component('coupon', {
	props: ['value'],

	computed: {
		code() { 
			return this.value 
		}
	},

	template: `
		<input class="input is-rounded" type="text" :value="code" @input="updateCode($event.target.value)" ref="input">
	`,

	methods: {
		updateCode(code) {
			if (code === "GRATIS") {
				alert('Nada es gratis en la vida.')

				this.$refs.input.value = code = '';



			} else if (code === "DESCUENTO") {
				alert("Ahora si negociemos...")
			}
			
			this.$emit('input', code)
		}
	}

});

new Vue({
	el:'#root',

	data: {
		dataCoupon: 'EXAMPLE'
	}


});