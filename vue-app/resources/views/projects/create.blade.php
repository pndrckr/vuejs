<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
    html, body {
        background-color: #fff;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        height: 100vh;
        margin: 0;
    }

    .full-height {
        height: 100vh;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

    .top-right {
        position: absolute;
        right: 10px;
        top: 18px;
    }

    .content {
        text-align: center;
    }

    .title {
        font-size: 84px;
    }

    .links > a {
        color: #636b6f;
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
    }

    .m-b-md {
        margin-bottom: 30px;
    }
    .errors {
        color: red;
    }
    .form-block {
        padding-bottom: 15px;
    }
</style>
</head>
<body>
    <div style="padding:20px 0 0 80px">
        @if (count($projects))
        <h1>My Projects:</h1>

        <ul>
            @foreach ($projects as $project)
            <li>
                <a href="#">{{ $project->name }}</a>
            </li>
            @endforeach
        </ul>

        @endif
    </div>

    <hr width="90%">

    <div id="vue-app" style="padding:20px 0 0 80px">

        <form method="POST" action="/projects" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
           <div class="form-block">
               <label for="name">Project Name:</label>
               <input type="text" class="input" name="name" id="name" v-model="form.name" @keydown="form.errors.clear('name')">
               <span class="errors" v-if="form.errors.has('name')" v-text="form.errors.get('name')"></span>
           </div>

           <div class="form-block">
               <label for="description"> Project Description:</label>
               <input type="text" class="input" name="description" id="description" v-model="form.description">
               <span class="errors" v-if="form.errors.has('description')" v-text="form.errors.get('description')"></span>
           </div>

           <div>
               <button :disabled="form.errors.any()">Create</button>
           </div>

       </form>

   </div>
   
   <button style="margin: 40px;"><a href="http://tati.learns.vue">Back HOME</a></button>

   <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
   <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
   <script src="/js/vue-app.js"></script>    
</body>
</html>
