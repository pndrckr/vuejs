class Errors {
    // Creo la instancia Errors
    constructor() {
        this.errors = {};
    }

    get(field) {
        if (this.errors[field]) {
            return this.errors[field][0];
        }
    }

    record(errors) {
        this.errors = errors;
    }

    clear(field){

        if (field) { 
            delete this.errors[field];
            return;
        }

        this.errors = {};

    }

    has(field) {
        return this.errors.hasOwnProperty(field);
    }

    any() {
        return Object.keys(this.errors).length > 0;
    }
}




class Form {
    constructor(data) {
        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }

        this.errors = new Errors();

    }

    data() {
        //  Creo objeto vacío
        let data =  {};

        for (let property in this.originalData) {
            data[property] = this[property];
            // Hace que se pueda acceder a ej: data.name como this.name
        }

        // Devuelvo lo que sea que se define en vue como new Form (data)
        return data;
    }

    submit(requestType, url) {
        return new Promise((res, rej) => {
            // axios response
            axios[requestType](url, this.data()) 
            .then(response => {
                this.onSuccess(response.data);
                res(response.data);
            })
            .catch(error => {
                this.onFail(error.response.data.errors);
                rej(error.response.data);
            });
        });
    }

    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }
        
        this.errors.clear();

    }

    onSuccess(data) {
        alert(data.message);
        
        this.reset();
    }

    onFail(errors) {
        this.errors.record(errors);
    }

}




new Vue({
    el: '#vue-app',

    data: {
        form: new Form({
            name: '',
            description: ''
        })
    },

    methods: {
        onSubmit() {
            this.form.submit('post', '/projects')
            .then(data => console.log(data))
            .catch(errors => console.log(errors));
        },

    }
});