new Vue({
	el:'#app',

	data: {
		skills: []
	},

	mounted() {

		// Voy a buscar en laravel /skills

		axios.get('/skills').then(response => this.skills = response.data);
		// axios.get('/skills').then(response => console.log(response.data));
	}
});
