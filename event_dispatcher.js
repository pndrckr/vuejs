window.Event = new class {
	constructor() {
		this.vue = new Vue();
	}

	fire(event, data = null) {
		this.vue.$emit(event, data = null);
	}

	listen(event, callback){
		this.vue.$on(event, callback);	
	}
}

Vue.component('coupon', {
	template: `
	<div class="field">	
		<label class="label">Please Enter your Coupon:</label>
    	<input class="input is-rounded" type="text" @blur="grito">
	</div>
	`,

	methods: {
		grito() {
			// Event.$emit('applied');
			Event.fire('applied');

			this.$on('applied', function() {

			})
		}
	}
});

new Vue({
	el:'#root',

	data: {
		couponApplied: false
	},

	created() {
		// Event.$on('applied', () => alert('aqui estoy'));
		Event.listen('applied', () => alert('aqui estoy'));
	}
});